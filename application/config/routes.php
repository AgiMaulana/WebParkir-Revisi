<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'LoginController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login']['post'] = 'LoginController/login';
$route['login']['get'] = 'LoginController/index';
$route['logout']['get'] = 'LoginController/logout';

/**
 * Admin
 */

$route['admin/dashboard']['get'] = 'HomeController/index';
$route['admin/company/add']['get'] = 'AdminHomeController/addCompanyView';
$route['admin/company/add/action']['post'] = 'AdminHomeController/addCompanyAction';
$route['admin/company/add/done']['get'] = 'AdminHomeController/registerSuccessView';
$route['admin/company/add/failed']['get'] = 'AdminHomeController/registerFailedView';
$route['admin/company/(:any)/confirmation/send']['get'] = 'AdminHomeController/resendConfirmationEmail';
$route['admin/company/(:any)/reset-password']['get'] = 'AdminHomeController/resetPasswordEmail';
$route['admin/company/(:any)/edit']['get'] = 'AdminHomeController/editCompanyView';
$route['admin/company/edit/action']['post'] = 'AdminHomeController/editCompanyAction';
$route['admin/company/(:any)/delete']['get'] = 'AdminHomeController/deleteCompany';

/**
 * Company
 */

$route['company/activation/(:any)']['get'] = 'CompanyAccountController/activate';
$route['company/edit/password']['get'] = 'CompanyAccountController/editPassword';
$route['company/edit/password']['post'] = 'CompanyAccountController/editPasswordAction';
$route['company/reset-password/(:any)']['get'] = 'CompanyAccountController/resetPasswordView';
$route['company/dashboard']['get'] = 'CompanyHomeController/index';
$route['company/location/add']['get'] = 'InputController/index';
$route['company/location/(:any)/edit']['get'] = 'HomeController/edit';
$route['company/location/(:any)/delete']['get'] = 'HomeController/hapus';
$route['company/location/(:any)/report']['get'] = 'CompanyHomeController/reportView';
$route['company/location/(:any)/report']['post'] = 'CompanyHomeController/reportView';
$route['company/location/(:any)/report/print']['get'] = 'CompanyHomeController/printView';
$route['company/profile']['get'] = 'CompanyHomeController/editCompanyView';

/**
 * Rest API
 */
$route['parking/slot/update']['post'] = 'ParkingController/updateSlot';
$route['parking/nearby-location']['get'] = 'ParkingController/nearbyLocations';
$route['parking/slot']['get'] = 'ParkingController/slotsDetail';
$route['parking/slot/status']['get'] = 'ParkingController/slotStatuses';
$route['parking/slot/delete']['post'] = 'ParkingController/deleteSlot';
$route['parking/parking/camera/delete']['post'] = 'ParkingController/deleteCamera';
$route['parking/parking/camera/rename']['post'] = 'ParkingController/renameCamera';

$route['user/update/token']['post'] = 'UserController/updateToken';
$route['user/update/location']['post'] = 'UserController/updateLocation';

//$route['user/tes']['get'] = 'UserController/broadcastToUser';
