<?php

/**
 * Created by PhpStorm.
 * User: agimaulana
 * Date: 14/06/17
 * Time: 23:33
 */
class AdminHomeController extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('CompanyHomeModel');
    }

    public function addCompanyView(){
        $this->load->view('template/nav');
        $this->load->view('template/header', array('title' => 'Tambah Perusahaan'));
        $this->load->view('admin/ViewRegisterCompany');
        $this->load->view('template/footer');
    }

    public function addCompanyAction(){
        $companyName = $this->input->post('company_name');
        $companyEmail = $this->input->post('email');
        $companyAddress = $this->input->post('address');

        $this->db->where("email", $companyEmail);
        if($this->db->count_all_results("company", false) > 0){
            redirect('admin/company/add/failed');
            return;
        }

        $companyId = $this->createCompanyId();
        $confirmationToken = self::createConfirmationToken($companyId, $companyEmail);
        $dbDatas = array(
            'company_id'=> $companyId,
            'name' => $companyName,
            'email' => $companyEmail,
            'address' => $companyAddress,
            'active' => 0,
            'confirmation_token' => $confirmationToken
        );
        $register = $this->db->insert('company', $dbDatas);
        if($register) {
            $this->sendConfirmationEmail($companyEmail, $confirmationToken);
            redirect('admin/company/add/done?id='. $companyId);
        }else{
            redirect('admin/company/add/failed');
        }
    }

    public function registerSuccessView(){
        $id = $this->input->get('id');
        if(!isset($id))
            redirect(base_url());

        $viewDatas = $this->db->select(array('name', 'email', 'address'))
            ->from('company')
            ->where('company_id', $id)
            ->get()
            ->row();

        $this->load->view('template/nav');
        $this->load->view('template/header', array('title' => 'Perusahaan Ditambahkan'));
        $this->load->view('admin/ViewRegisterCompanyDone', $viewDatas);
        $this->load->view('template/footer');
    }

    public function registerFailedView(){
        $this->load->view('template/nav');
        $this->load->view('template/header', array('title' => 'Pendaftaran Gagal'));
        $this->load->view('admin/ViewRegisterCompanyFailed');
        $this->load->view('template/footer');
    }

    public function editCompanyView(){
        $company_id = $this->uri->segment(3);
        $this->db->where("company_id", $company_id);
        if($this->db->count_all_results("company", false) == 0){
            redirect(base_url());
            return;
        }
        $this->db->reset_query();

        $title['title'] = 'Sunting Perusahaan';
        $where = array('company_id' => $company_id);
        $data = $this->db->select(['company_id','name', 'email','address'])
            ->from('company')
            ->where($where)
            ->get()
            ->row();

        $this->load->view('template/nav');
        $this->load->view('template/header', array('title' => 'Sunting Perusahaan'));
        $this->load->view('admin/ViewEditCompany', $data);
        $this->load->view('template/footer');
    }

    public function editCompanyAction(){
        $companyId = $this->input->post('company_id');
        $companyName = $this->input->post('company_name');
        $companyEmail = $this->input->post('email');
        $companyAddress = $this->input->post('address');

        $this->db->where("email", $companyEmail);
        if($this->db->count_all_results("company", false) == 0
            || !isset($companyId, $companyName, $companyEmail, $companyAddress)){
            redirect('admin/company/add/failed');
            return;
        }
        $dbDatas = array(
            'name' => $companyName,
            'email' => $companyEmail,
            'address' => $companyAddress
        );
        $this->db->where('company_id', $companyId)
            ->update('company', $dbDatas);
        redirect(base_url());
    }

    public function resendConfirmationEmail(){
        $companyId = $this->uri->segment(3);
        $this->db->where("company_id", $companyId);
        if($this->db->count_all_results("company", false) == 0){
            redirect(base_url());
            return;
        }
        $this->db->reset_query();

        $company = $this->db->select('*')
            ->from('company')
            ->where('company_id', $companyId)
            ->get()
            ->row();
        $confirmationToken = self::createConfirmationToken($companyId, $company->email);
        $updateData = array(
            'confirmation_token' => $confirmationToken
        );
        $this->db->where('company_id', $companyId)
            ->update('company', $updateData);
        $this->sendConfirmationEmail($company->email, $confirmationToken);

        $data['title'] = "Email Konfirmasi Terkirim";
        $data['message'] = "Perusahaan aktif setelah email dikonfirmasi.\n
                                    Lihat menu &le;b&gt;;Daftar Perusahaan&le;/b&gt;; untuk mengirim ulang tautan konfirmasi.";
        $data['email'] = $company->email;

        $this->load->view('template/nav');
        $this->load->view('template/header', array('title' => 'Email Konfirmasi Terkirim'));
        $this->load->view('admin/ViewConfirmationEmailSent', $data);
        $this->load->view('template/footer');
    }

    public function resetPasswordEmail(){
        $companyId = $this->uri->segment(3);
        $this->db->where("company_id", $companyId);
        if($this->db->count_all_results("company", false) == 0){
            redirect(base_url());
            return;
        }
        $this->db->reset_query();

        $company = $this->db->select('*')
            ->from('company')
            ->where('company_id', $companyId)
            ->get()
            ->row();
        $confirmationToken = self::createConfirmationToken($companyId, $company->email);
        $updateData = array(
            'confirmation_token' => $confirmationToken
        );
        $this->db->where('company_id', $companyId)
            ->update('company', $updateData);
        $this->sendResetPasswordEmail($company->email, $confirmationToken);

        $data['title'] = "Email Terkirim";
        $data['message'] = "Tautan untuk mereset password terkirim.";
        $data['email'] = $company->email;


        $this->load->view('template/nav');
        $this->load->view('template/header', array('title' => 'Email Terkirim'));
        $this->load->view('admin/ViewConfirmationEmailSent', $data);
        $this->load->view('template/footer');
    }

    public function deleteCompany(){
        $companyId = $this->uri->segment(3);
        $this->db->where("company_id", $companyId);
        if($this->db->count_all_results("company", false) == 0){
            redirect(base_url());
            return;
        }
        $this->db->reset_query();
        $this->CompanyHomeModel->deleteLocations($companyId);
        $this->db->where('company_id', $companyId)
            ->delete('company');
        redirect(base_url('admin/dashboard'));
    }

    private function sendConfirmationEmail($email, $confirmationToken){
        $this->load->library('email');

        $this->email->from('admin@parkingmonitoring.kontrakanproject.com', 'Parking Monitoring Admin');
        $this->email->to($email);
//        $this->email->cc('another@another-example.com');
//        $this->email->bcc('them@their-example.com');

        $this->email->subject('Company Activation');
        $this->email->message("Click below link to activate your company account in Smart Parking Monitoring.
        
        ".  base_url("/company/activation/". $confirmationToken) ."
        
        The link just active at once. If the link not work, ask the Administrator to resend confirmation link.");

        $this->email->send();
    }

    private function sendResetPasswordEmail($email, $confirmationToken){
        $this->load->library('email');

        $this->email->from('admin@parkingmonitoring.kontrakanproject.com', 'Parking Monitoring Admin');
        $this->email->to($email);
//        $this->email->cc('another@another-example.com');
//        $this->email->bcc('them@their-example.com');

        $this->email->subject('Reset Password');
        $this->email->message("Click below link to reset your company account password in Smart Parking Monitoring.
        
        ".  base_url("/company/reset-password/". $confirmationToken) ."
        
        The link just active at once. If the link not work, ask the Administrator to resend the link.");

        $this->email->send();
    }

    private function createCompanyId(){
        $alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $numb = "0123456789";
        $id = $alpha[rand(0, strlen($alpha)-1)];
        for ($i = 0; $i < 5; $i++){
            $rnd = rand(0, strlen($numb)-1);
            $id.=$numb[$rnd];
        }

        $this->db->reset_query();
        $this->db->where("company_id", $id);
        if($this->db->count_all_results("company", false) == 0)
            return $id;
        return $this->createCompanyId();
    }

    public static function createConfirmationToken($companyId, $email){
        $alpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $id = "";
        for ($i = 0; $i < 32; $i++){
            $rnd = rand(0, strlen($alpha)-1);
            $id.=$alpha[$rnd];
        }

        $key = hash('sha1', $companyId. $email);
        return hash('sha1', $key.$id);
    }
}