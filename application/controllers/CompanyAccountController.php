<?php

/**
 * Created by PhpStorm.
 * User: agimaulana
 * Date: 16/06/17
 * Time: 7:14
 */
class CompanyAccountController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function activate(){
        $token =  $this->uri->segment(3);
        if(!isset($token)){
            redirect(base_url());
            return;
        }

        $this->db->where("confirmation_token", $token);
        if($this->db->count_all_results("company", false) == 0){
            $this->load->view('company/ViewRegisterActivationExpired');
            return;
        }
        $this->db->reset_query();

        $company = $this->db->select('*')
            ->from('company')
            ->where('confirmation_token', $token)
            ->get()
            ->row();

        $authToken = $this->createAuthToken($company->company_id, $company->email);
        $updateData = array(
            'token' => $authToken,
            'active' => 1,
            'confirmation_token' => ""
        );
        $this->db->where('confirmation_token', $token)
            ->update('company', $updateData);

        $newdata = array(
            'auth_token'  => $authToken,
            'as' => 'company'
        );

        $this->session->set_userdata($newdata);

        $this->load->view('company/ViewRegisterActivation');
    }

    public function editPasswordAction(){
        $token = $this->session->userdata('auth_token');
        $as = $this->session->userdata('as');

        $this->db->where("token", $token);
        if($this->db->count_all_results("company", false) == 0){
            redirect(base_url('logout'));
            return;
        }
        $this->db->reset_query();

        $password = $this->input->post('password');
        $updateData = array(
            'password' => hash('sha1',$password)
        );
        $this->db->where('token', $token)
            ->update('company', $updateData);
        redirect(base_url());
    }

    public function resetPasswordView(){
        $token =  $this->uri->segment(3);
        if(!isset($token)){
            redirect(base_url());
            return;
        }

        $this->db->where("confirmation_token", $token);
        if($this->db->count_all_results("company", false) == 0){
            $this->load->view('company/ViewRegisterActivationExpired');
            return;
        }
        $this->db->reset_query();

        $company = $this->db->select('*')
            ->from('company')
            ->where('confirmation_token', $token)
            ->get()
            ->row();

        $authToken = $this->createAuthToken($company->company_id, $company->email);
        $updateData = array(
            'token' => $authToken,
            'active' => 1,
            'confirmation_token' => ""
        );
        $this->db->where('confirmation_token', $token)
            ->update('company', $updateData);

        $newdata = array(
            'auth_token'  => $authToken,
            'as' => 'company'
        );

        $this->session->set_userdata($newdata);

        $this->load->view('company/ViewResetPassword');
    }

    private function createAuthToken($companyId, $email){
        $alpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $id = "";
        for ($i = 0; $i < 32; $i++){
            $rnd = rand(0, strlen($alpha)-1);
            $id.=$alpha[$rnd];
        }

        $key = hash('sha1', $companyId. $email);
        return hash('sha1', $key.$id);
    }
}