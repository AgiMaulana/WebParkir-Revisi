<?php

/**
 * Created by PhpStorm.
 * User: agimaulana
 * Date: 18/06/17
 * Time: 12:37
 */
class CompanyHomeController extends CI_Controller {

    function __construct(){
        parent::__construct();
        if(!$this->session->has_userdata('auth_token'))
            redirect(base_url('logout'));
        $this->load->model('CompanyHomeModel');
    }

    public function index()
    {
        $token = $this->session->userdata('auth_token');
        $as = $this->session->userdata('as');

        $this->db->where("token", $token);
        if($this->db->count_all_results("company", false) == 0){
            redirect(base_url('logout'));
            return;
        }
        $this->db->reset_query();

        $title['title']= 'Dashboard';
        $data['company'] = $this->CompanyHomeModel->locationsData();
        $this->load->view('company/template/nav', $this->CompanyHomeModel->navBadge());
        $this->load->view('company/template/header',$title);
        $this->load->view('company/ViewDashboard', $data);
        $this->load->view('company/template/footer');
    }

    public function reportView(){
        $token = $this->session->userdata('auth_token');
        $as = $this->session->userdata('as');

        $this->db->where("token", $token);
        if($this->db->count_all_results("company", false) == 0){
            redirect(base_url('logout'));
            return;
        }
        $this->db->reset_query();
        $location_id = $this->uri->segment(3);

        $d = $this->input->get("date", true);
        if(!isset($d) || empty($d))
            $d = date("Y-m-d");

        $data['location'] = $this->db->select('*')
            ->from('lokasi')
            ->where('id_lokasi', $location_id)
            ->get()
            ->row();
        $data['location_id'] = $location_id;
        $data['date'] = $d;
        $data['slots'] = $this->CompanyHomeModel->slots($location_id);

        $title['title']= 'Laporan';
        $this->load->view('company/template/header',$title);
        $this->load->view('company/template/nav', $this->CompanyHomeModel->navBadge());
        $this->load->view('company/ViewReport', $data);
        $this->load->view('company/template/footer');
    }

    public function getSlots(){
        $locationId = $this->input->get('location_id');
        echo json_encode($this->CompanyHomeModel->slots($locationId));
    }

    public function printView(){
        $token = $this->session->userdata('auth_token');
        $as = $this->session->userdata('as');

        $this->db->where("token", $token);
        if($this->db->count_all_results("company", false) == 0){
            redirect(base_url('logout'));
            return;
        }
        $this->db->reset_query();
        $location_id = $this->uri->segment(3);

        $d = $this->input->get("date", true);
        if(!isset($d) || empty($d))
            $d = date("Y-m-d");

        $data['location'] = $this->db->select('*')
            ->from('lokasi')
            ->where('id_lokasi', $location_id)
            ->get()
            ->row();
        $data['location_id'] = $location_id;
        $data['date'] = $d;
        $data['slots'] = $this->CompanyHomeModel->slots($location_id);

        $title['title']= 'Laporan';
        $this->load->view('company/ViewPrint', $data);
    }

    public function editCompanyView(){
        $token = $this->session->userdata('auth_token');
        $as = $this->session->userdata('as');

        $this->db->where("token", $token);
        if($this->db->count_all_results("company", false) == 0){
            redirect(base_url('logout'));
            return;
        }
        $this->db->reset_query();

        $title['title'] = 'Sunting Perusahaan';
        $where = array('token' => $token);
        $data = $this->db->select(['company_id','name', 'email','address'])
            ->from('company')
            ->where($where)
            ->get()
            ->row();

        $this->load->view('template/header', array('title' => 'Sunting Perusahaan'));
        $this->load->view('template/nav');
        $this->load->view('admin/ViewEditCompany', $data);
        $this->load->view('template/footer');
    }
}