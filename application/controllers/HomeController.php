<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

	function __construct(){
		parent::__construct();
        if(!$this->session->has_userdata('auth_token'))
            redirect(base_url('logout'));
		$this->load->model('HomeModel');
	}

	public function index()
	{
		$title['title']= 'Dashboard';
		$data['company'] = $this->HomeModel->tampil_data();
		$this->load->view('template/nav');
		$this->load->view('template/header',$title);
		$this->load->view('viewHome', $data);
		$this->load->view('template/footer'); 
	}

    function edit(){
        $token = $this->session->userdata('auth_token');
        $as = $this->session->userdata('as');

        $this->db->where("token", $token);
        if($this->db->count_all_results("company", false) == 0){
            redirect(base_url('logout'));
            return;
        }
        $this->db->reset_query();

        $id_lokasi = $this->uri->segment(3);

        $title['title']= 'Ubah Data Lokasi';
        $where = array('id_lokasi' => $id_lokasi);
        $data['lokasi'] = $this->HomeModel->edit_data($where,'lokasi')->result();
        $this->load->view('template/nav');
        $this->load->view('template/header',$title);
        $this->load->view('viewEdit',$data);
        $this->load->view('template/footer');
    }

    function update(){
        $token = $this->session->userdata('auth_token');
        $as = $this->session->userdata('as');

        $this->db->where("token", $token);
        if($this->db->count_all_results("company", false) == 0){
            redirect(base_url('logout'));
            return;
        }
        $this->db->reset_query();

        $id_lokasi = $this->input->post('id_lokasi');
        $nama_lokasi = $this->input->post('nama_lokasi');
        $kota = $this->input->post('kota');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        $alamat = $this->input->post('alamat');

        $data = array(
            'id_lokasi'    => $id_lokasi,
            'nama_lokasi'    => $nama_lokasi,
            'kota'    => $kota,
            'latitude'    => $latitude,
            'longitude'    => $longitude,
            'alamat'        => $alamat
        );

        $where = array(
            'id_lokasi' => $id_lokasi
        );

//        $this->HomeModel->update_data($where,$data,'lokasi');
        $this->db->set($data)
            ->where($where)
            ->update('lokasi');
        redirect(base_url('company/dashboard'));
    }

    function hapus(){
        $token = $this->session->userdata('auth_token');
        $as = $this->session->userdata('as');

        $this->db->where("token", $token);
        if($this->db->count_all_results("company", false) == 0){
            redirect(base_url('logout'));
            return;
        }
        $this->db->reset_query();

        $id_lokasi = $this->uri->segment(3);

        $userIds = $this->db->select('user.id')
            ->from('user')
            ->join('user_listen', 'user_listen.id_user = user.id')
            ->join('slot', 'user_listen.id_location = slot.id_location')
            ->where('slot.id_location', $id_lokasi)
            ->group_by('user.id')
            ->get()
            ->result();

        $this->db->where('id_location', $id_lokasi)
            ->delete('slot');
        $where = array('id_lokasi' => $id_lokasi);
        $this->HomeModel->hapus_data($where,'lokasi');

        $this->load->model('ListenerBroadcaster');
        foreach ($userIds as $key => $value){
            $broadcaster = new ListenerBroadcaster();
            $broadcaster->broadcastToUser($value->id);
        }
        redirect(base_url('company/dashboard'));
    }
}
