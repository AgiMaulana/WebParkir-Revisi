<?php

class InputController extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model("HomeModel");
		$this->load->model("CompanyHomeModel");
	}
	
	public function index()
	{
		$title['title']= 'Input Lokasi Parkir';
		$table = 'lokasi';
		$data['aksi'] = 'act_add';
		$this->load->view('company/template/nav', $this->CompanyHomeModel->navBadge());
		$this->load->view('company/template/header',$title);
		$this->load->view('viewInput',$data);
		$this->load->view('company/template/footer');
	}

    public function aksi(){
        $companyId = $this->CompanyHomeModel->getCompanyId();
        $aksi = $this->uri->segment('3');
        #print $aksi;
        $table = 'lokasi';
        $iduri = $this->uri->segment('4');

        $id = $this->createParkingAreaId();
        if($aksi =='act_add'){
            $data = array(
                'id_lokasi'    => $id,
                'nama_lokasi'    => $this->input->post('nama_lokasi'),
                'kota'    => $this->input->post('kota'),
                'latitude'    => $this->input->post('latitude'),
                'longitude'    => $this->input->post('longitude'),
                'alamat'        => $this->input->post('alamat'),
                'company_id'    => $companyId
            );
            $this->db->insert($table,$data);

            $data['title']= 'Berhasil';
            $data['aksi'] = 'act_add';
            $data['kode'] = $id;
            redirect("InputController/addDone?id=".$id);

        }
    }

    public function addDone(){
        $this->load->view('template/nav');
        $this->load->view('template/header',array('title'=>'Selesai'));
        $this->load->view('viewSelesai',array('kode'=>$this->input->get('id')));
        $this->load->view('template/footer');
    }

    function delete(){
        $nama_lokasi = $this->uri->segment(4);
        $this->db->where('nama_lokasi', $nama_lokasi);
        $this->db->delete('lokasi');
        redirect('HomeController');
        echo "";
    }

    private function createParkingAreaId(){
        $numb = "0123456789";
        $id = "P";
        for ($i = 0; $i < 5; $i++){
            $rnd = rand(0, strlen($numb)-1);
            $id.=$numb[$rnd];
        }

        $this->db->where("id_lokasi", $id);
        if($this->db->count_all_results("lokasi", false) == 0)
            return $id;
        return $this->createParkingAreaId();
    }
}