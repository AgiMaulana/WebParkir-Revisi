<?php

class LoginController extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('LoginModel');
	}
	
	function index(){
        $token = $this->session->userdata('auth_token');
        $as = $this->session->userdata('as');

        if(strcmp($as, 'admin') == 0){
            redirect(base_url('admin/dashboard'));
        }else if(strcmp($as, 'company') == 0){
            redirect(base_url('company/dashboard'));
        }else {
            $this->load->view("viewLogin", array('isFailed' => false));
        }
	}
	
	function login(){
		$user = $this->input->post("user");
		$password = $this->input->post("password");

        if( strpos( $user, "parkingmonitoring.kontrakanproject.com" ) !== false ) {
            $login = $this->LoginModel->loginAsAdmin($user, $password);
            if($login['status'])
                redirect(base_url());
            else
                $this->load->view("viewLogin", array('isFailed' => true));
        }else{
            $login = $this->LoginModel->loginAsCompany($user, $password);
            if($login['status'])
                redirect(base_url());
            else
                $this->load->view("viewLogin", array('isFailed' => true));
        }
        return;
	}
	
	function logout(){
	    $token = $this->session->userdata('auth_token');
	    $as = $this->session->userdata('as');
        if(!isset($token, $as)){
            redirect(base_url());
            return;
        }

        $where = array('token' => $token);
        $update = array('token' => '');

        if(strcmp($as, 'admin') == 0){
            $this->db->where($where)
                ->update('admin', $update);
        }else{
            $this->db->where($where)
                ->update('admin', $update);
        }

        $this->session->unset_userdata('auth_token');
        $this->session->unset_userdata('as');
        session_destroy();
		redirect(base_url());
	}
	
}