<?php

/**
 * Created by PhpStorm.
 * User: agimaulana
 * Date: 17/04/17
 * Time: 8:40
 */

class ParkingController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('LatLng');
    }

    public function updateSlot(){
        $parkingId = $this->input->post("parking_id");
        $slotId = $this->input->post("slot_id");
        $cameraIp = $this->input->post("camera_ip");
        $cameraName = $this->input->post("camera_name");
        $datetime = $this->input->post("datetime");
        $availablity = $this->input->post("availablity");

        if(!isset($parkingId, $slotId, $cameraIp, $cameraName, $datetime, $availablity)){
            echo json_encode(array(
                "status" => false,
                "message" => "missing param",
                "post_data" => $this->input->post()
            ));
            return;
        }

        /*$isLocationFound = $this->db->select("id_lokasi")
            ->from("lokasi")
            ->where('id_lokasi', $parkingId)
            ->get()
            ->num_rows();

        if($isLocationFound == 0){
            echo json_encode(array(
                "status" => false,
                "message" => "Lokasi parkir tidak ditemukan"
            ));
            return;
        }

        $where = array(
            "id_location" => $parkingId,
            "id_slot" => $slotId
        );
        $row = $this->db->select("id")
            ->from("slot")
            ->where("id_location", $parkingId)
            ->where("id_slot", $slotId)
            ->where('camera_ip', $cameraIp)
            ->get();

        $data = array(
            'camera_name' => $cameraName,
            'datetime' => $datetime,
            'availablity' => $availablity
        );

        if($row->num_rows() == 0){
            $data['camera_ip'] = $cameraIp;
            $data["id_location"] = $parkingId;
            $data["id_slot"] = $slotId;
            $result = $this->db->insert("slot", $data);
        }else{
            $where['camera_ip'] = $cameraIp;
            $result = $this->db->set($data)
                ->where($where)
                ->update("slot");
        }*/

//        $availablities = array();
//        if(strpos($availablity, ",")  !== false)
//            $availablities =  explode(",", $availablity);
//
//        $datetimes = array();
//        if(strpos($datetime, ",")  !== false)
//            $datetimes =  explode(",", $datetime);

        if(strpos($slotId, ",")  !== false){
            $slotIds = explode(",", $slotId);
            $availablities =  explode(",", $availablity);
            $datetimes =  explode(",", $datetime);
            for ($i = 0; $i<sizeof($slotIds); $i++){
                if(strpos($availablity, ",")  !== false) {
                    $result[] = $this->insertOrUpdateSlot($parkingId, $slotIds[$i], $cameraIp, $cameraName, $datetimes[$i], $availablities[$i]);
                }else {
                    $result[] = $this->insertOrUpdateSlot($parkingId, $slotIds[$i], $cameraIp, $cameraName, $datetime, $availablity);
                }
            }
            $slotId = $slotIds[0];
        }else{
            $result = $this->insertOrUpdateSlot($parkingId, $slotId, $cameraIp, $cameraName, $datetime,$availablity);
        }

        $userIds = $this->db->select('user.id')
            ->from('user')
            ->join('user_listen', 'user_listen.id_user = user.id')
            ->join('slot', 'user_listen.id_location = slot.id_location')
            ->where('slot.id_location', $parkingId)
            ->where('slot.id_slot', $slotId)
            ->group_by('user.id')
            ->get()
            ->result();
        foreach ($userIds as $key => $value){
            $this->load->model('ListenerBroadcaster');
            $broadcaster = new ListenerBroadcaster();
            $broadcaster->broadcastToUser($value->id);
        }

        if(!is_array($result)){
            if($result){
                echo json_encode(array(
                    "status" => true,
                    "message" => "slot updated"
                ));
            }else{
                echo json_encode(array(
                    "status" => false,
                    "message" => "failed to update slot"
                ));
            }
        }else{
            echo json_encode(array(
                "status" => true,
                "message" => json_encode($result)
            ));
        }
    }

    private function insertOrUpdateSlot($parkingId, $slotId, $cameraIp, $cameraName, $datetime, $availablity){
        $isLocationFound = $this->db->select("id_lokasi")
            ->from("lokasi")
            ->where('id_lokasi', $parkingId)
            ->get()
            ->num_rows();

        if($isLocationFound == 0){
            echo json_encode(array(
                "status" => false,
                "message" => "Lokasi parkir tidak ditemukan"
            ));
            return false;
        }

        $where = array(
            "id_location" => $parkingId,
            "id_slot" => $slotId
        );

        $data = array(
            'camera_name' => $cameraName,
            'datetime' => $datetime,
            'availablity' => $availablity
        );

        $row = $this->db->select("id")
            ->from("slot")
            ->where("id_location", $parkingId)
            ->where("id_slot", $slotId)
            ->where('camera_ip', $cameraIp)
            ->get();

        if($row->num_rows() == 0){
            $data['camera_ip'] = $cameraIp;
            $data["id_location"] = $parkingId;
            $data["id_slot"] = $slotId;
            $result = $this->db->insert("slot", $data);
            $this->db->insert('slot_history', $data);

            return $result;
        }else{
            $row = $this->db->select("availablity")
                ->from("slot")
                ->where("id_location", $parkingId)
                ->where("id_slot", $slotId)
                ->where('camera_ip', $cameraIp)
                ->get()
                ->row();
            if($row->availablity != $availablity) {
                $where['camera_ip'] = $cameraIp;
                $result = $this->db->set($data)
                    ->where($where)
                    ->update("slot");
                $data['camera_ip'] = $cameraIp;
                $data["id_location"] = $parkingId;
                $data["id_slot"] = $slotId;
                $this->db->insert('slot_history', $data);

                return $result;
            }
            return false;
        }
    }

    private function broadcastToListener($parkingId){
        $listeners = $this->db->select('user.id, user.fcm_token', 'user_listence.radius')
            ->from('user')
            ->join('user_listen', 'user.id = user_listen.id_user')
            ->where('user_listen.id_location', $parkingId)
            ->get()
            ->result();

        foreach ($listeners as $key => $value){
            $locations = $this->db->select('*')
                ->from('lokasi')
                ->join('user_listen', 'user_listen.id_location = lokasi.id_lokasi')
                ->where('user_listen.id_user', $value->id_user)
                ->get()
                ->result();

            $latLng = new LatLng();
            $latLng->setLatLng($value->latitude, $value->longitude);
            $result = array();
            foreach ($locations as $key2 => $value2){
                $destLatLng = new LatLng();
                $destLatLng->setLatLng($value2->latitude, $value2->longitude);
                $distance = $latLng->distanceTo($destLatLng)['distance'];
                $distance = str_replace(",",".", $distance);
                if(strpos($distance, 'km')){
                    $distance = $distance * 1000;
                }
                if($distance <= $value->radius) {
                    $totalSlots = $this->db->select('id')
                        ->from('slot')
                        ->where('id_location', $value->id_lokasi)
                        ->get()
                        ->num_rows();
                    $availableSlots = $this->db->select('id')
                        ->from('slot')
                        ->where('id_location', $value->id_lokasi)
                        ->where('availablity', 0)
                        ->get()
                        ->num_rows();

                    $value->latitude = (float) $value->latitude;
                    $value->longitude = (float) $value->longitude;
                    $value->distance = $distance;
                    $value->slot['total'] = $totalSlots;
                    $value->slot['available'] = $availableSlots;
                    $result[] = $value;
                }
            }
            $response = array(
                'status' => true,
                'message' => 'nearby locations',
                'data' => array(
//                    'origin_latlng' => $latLng->toString(),
//                    'origin_city' => $addresses['city'],
                    'total' => sizeof($result),
                    'locations' => $result
                )
            );

            echo json_encode($response);
            return;
        }
    }

    public function nearbyLocations(){
        $lat = $this->input->get("lat");
        $lng = $this->input->get("lng");
        $radius = $this->input->get('radius');

        if(!isset($lat, $lng, $radius)){
            echo json_encode(array(
                "status" => false,
                "message" => "Missing params"
            ));
            return;
        }

        $this->load->library('LatLng');
        $latLng = new LatLng();
        $latLng->setLatLng($lat, $lng);
        $addresses = $latLng->getAddress();
        $locations = $this->db->select('*')
            ->from('lokasi')
            ->where('kota', $addresses['city'])
            ->get()
            ->result();

        $result = array();
        foreach ($locations as $key => $value){
            $destLatLng = new LatLng();
            $destLatLng->setLatLng($value->latitude, $value->longitude);
            $distance = $destLatLng->distanceTo($latLng)['distance'];
            $distance = str_replace(",",".", $distance);
            if(strpos($distance, 'km')){
                $distance = $distance * 1000;
            }
            if($distance <= $radius) {
                $totalSlots = $this->db->select('id')
                    ->from('slot')
                    ->where('id_location', $value->id_lokasi)
                    ->get()
                    ->num_rows();
                $availableSlots = $this->db->select('id')
                    ->from('slot')
                    ->where('id_location', $value->id_lokasi)
                    ->where('availablity', 0)
                    ->get()
                    ->num_rows();

                $value->latitude = (float) $value->latitude;
                $value->longitude = (float) $value->longitude;
                $value->distance = $distance;
                $value->slot['total'] = $totalSlots;
                $value->slot['available'] = $availableSlots;
                $result[] = $value;
            }
        }
        $response = array(
            'status' => true,
            'message' => 'nearby locations',
            'data' => array(
                'origin_latlng' => $latLng->toString(),
                'origin_city' => $addresses['city'],
                'total' => sizeof($result),
                'locations' => $result
            )
        );

        echo json_encode($response);
        return;
    }

    public function slotsDetail(){
        $parkingId = $this->input->get("parking");
        if(!isset($parkingId)){
            echo json_encode(array(
                "status" => false,
                "message" => "Missing params"
            ));
            return;
        }

        $isLocationAvailable = $this->db->select('id_lokasi')
            ->from('lokasi')
            ->where('id_lokasi', $parkingId)
            ->get()
            ->num_rows();
        if($isLocationAvailable == 0){
            echo json_encode(array(
                "status" => false,
                "message" => "Lokasi tidak ditemukan"
            ));
            return;
        }

        $cameras = $this->db->select('camera_name')
            ->from('slot')
            ->where('id_location', $parkingId)
            ->group_by('camera_name')
            ->order_by('camera_name', 'ASC')
            ->get()
            ->result();

        $datas = array();
        foreach ($cameras as $key => $value){
            $slots = $this->db->select(array('id_slot', 'id_location', 'datetime', 'availablity'))
                ->from('slot')
                ->where('id_location', $parkingId)
                ->where('camera_name', $value->camera_name)
                ->order_by('id_slot', 'ASC')
                ->get()
                ->result();

            $availableCount = $this->db->select('id')
                ->from('slot')
                ->where('id_location', $parkingId)
                ->where('camera_name', $value->camera_name)
                ->where('availablity', 0)
                ->get()
                ->num_rows();
            $no = 1;
            foreach ($slots as $sl) {
                $sl->number = $no;
                $no++;
            }

            $slotsSize = sizeof($slots);
            $result['camera_name'] = $value->camera_name;
            $result['available'] = $availableCount;
            $result['filled'] = $slotsSize - $availableCount;
            $result['total_slot'] = $slotsSize;
            $result['slots'] = $slots;
            $datas[] = $result;
        }

        echo json_encode(array(
            "status" => true,
            "message" => "Slot untuk lokasi ". $parkingId,
            "data" => $datas
        ));
        return;
    }

    public function slotStatuses(){
        $parkingId = $this->input->get('parking');
        $cameraName = $this->input->get('camera');
        if(!isset($parkingId, $cameraName)){
            echo json_encode(array(
                "status" => false,
                "message" => "Missing params"
            ));
            return;
        }

        $cameras = $this->db->select('camera_name')
            ->from('slot')
            ->where('id_location', $parkingId)
            ->where('camera_name', $cameraName)
            ->group_by('camera_name')
            ->order_by('camera_name', 'ASC')
            ->get()
            ->num_rows();
        if($cameras == 0){
            echo json_encode(array(
                "status" => false,
                "message" => "Kamera tidak ditemukan"
            ));
            return;
        }

        $datas = array();
        $slots = $this->db->select(array('id_slot', 'id_location', 'datetime', 'availablity'))
            ->from('slot')
            ->where('id_location', $parkingId)
            ->where('camera_name', $cameraName)
            ->order_by('id_slot', 'ASC')
            ->get()
            ->result();

        $availableCount = $this->db->select('id')
            ->from('slot')
            ->where('id_location', $parkingId)
            ->where('camera_name', $cameraName)
            ->where('availablity', 0)
            ->get()
            ->num_rows();
        $no = 1;
        foreach ($slots as $sl) {
            $sl->number = $no;
            $no++;
        }

        $slotsSize = sizeof($slots);
        $result['camera_name'] = $cameraName;
        $result['available'] = $availableCount;
        $result['filled'] = $slotsSize - $availableCount;
        $result['total_slot'] = $slotsSize;
        $result['slots'] = $slots;
        $datas[] = $result;

        echo json_encode(array(
            "status" => true,
            "message" => "Slot untuk lokasi ". $parkingId,
            "data" => $datas
        ));
        return;
    }

    public function deleteSlot(){
        $parkingId = $this->input->post('parking');
        $slotId = $this->input->post('slot');
        if(!isset($parkingId, $slotId)){
            echo json_encode(array(
                "status" => false,
                "message" => "Missing params"
            ));
            return;
        }

        $userId = $this->db->select('user.id')
            ->from('user')
            ->join('user_listen', 'user_listen.id_user = user.id')
            ->where('user_listen.id_location', $parkingId)
            ->group_by('user.id')
            ->get()
            ->row()
            ->id;

        $delete = $this->db->delete('slot', array('id_location' => $parkingId,'id_slot' => $slotId));
        $this->load->model('ListenerBroadcaster');
        $broadcaster = new ListenerBroadcaster();
        $broadcaster->broadcastToUser($userId);
        if($delete){
            echo json_encode(array(
                "status" => true,
                "message" => "slot deleted"
            ));
        }else{
            echo json_encode(array(
                "status" => false,
                "message" => "Slot delete failed"
            ));
        }
        return;
    }

    public function deleteCamera(){
        $parkingId = $this->input->post('parking');
        $cameraIp = $this->input->post('camera');
        if(!isset($parkingId, $cameraIp)){
            echo json_encode(array(
                "status" => false,
                "message" => "Missing params"
            ));
            return;
        }

        $count = $this->db->select('camera_ip')
            ->from('slot')
            ->where(array('id_location' => $parkingId,'camera_ip' => $cameraIp))
            ->get()
            ->num_rows();
        if($count == 0){
            echo json_encode(array(
                "status" => false,
                "message" => "Not found"
            ));
            return;
        }

        /*$userId = $this->db->select('user.id')
            ->from('user')
            ->join('user_listen', 'user_listen.id_user = user.id')
            ->where('user_listen.id_location', $parkingId)
            ->group_by('user.id')
            ->get()
            ->row()
            ->id;*/

        $delete = $this->db->delete('slot', array('id_location' => $parkingId,'camera_ip' => $cameraIp));
        if($delete){
            /*$this->load->model('ListenerBroadcaster');
            $broadcaster = new ListenerBroadcaster();
            $broadcaster->broadcastToUser($userId);*/

            echo json_encode(array(
                "status" => true,
                "message" => "Camera deleted"
            ));
        }else{
            echo json_encode(array(
                "status" => false,
                "message" => "Camera delete failed"
            ));
        }
        return;
    }

    public function renameCamera(){
        $parkingId = $this->input->post('parking');
        $cameraIpOld = $this->input->post('camera_old');
        $cameraIp = $this->input->post('camera');
        $cameraName = $this->input->post('camera_name');
        if(!isset($parkingId, $cameraIp, $cameraName)){
            echo json_encode(array(
                "status" => false,
                "message" => "Missing params"
            ));
            return;
        }

        $count = $this->db->select('camera_ip')
            ->from('slot')
            ->where('id_location', $parkingId)
            ->where('camera_ip', $cameraIpOld)
            ->get()
            ->num_rows();
        if($count == 0){
            echo json_encode(array(
                "status" => false,
                "message" => "Not found"
            ));
            return;
        }

        $update = $this->db->set(array(
                'camera_ip' => $cameraIp,
                'camera_name' => $cameraName
            ))
            ->where('id_location', $parkingId)
            ->where('camera_ip', $cameraIpOld)
            ->update('slot');
        if($update){
            $userId = $this->db->select('user.id')
                ->from('user')
                ->join('user_listen', 'user_listen.id_user = user.id')
                ->where('user_listen.id_location', $parkingId)
                ->group_by('user.id')
                ->get()
                ->row()
                ->id;
            $this->load->model('ListenerBroadcaster');
            $broadcaster = new ListenerBroadcaster();
            $broadcaster->broadcastToUser($userId);

            echo json_encode(array(
                "status" => true,
                "message" => "Camera updated"
            ));
        }else{
            echo json_encode(array(
                "status" => false,
                "message" => "Camera update failed"
            ));
        }
        return;
    }
}