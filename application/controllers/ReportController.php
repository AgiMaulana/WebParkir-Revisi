<?php

/**
 * Created by PhpStorm.
 * User: mayang kusuma
 * Date: 4/28/2017
 * Time: 9:56 PM
 */
class ReportController extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model("ReportModel");
    }

    public function index()
    {
        $title['title']= 'Home';
        $data['slot_history'] = $this->ReportModel->tampil_data();
        $this->load->view('template/nav');
        $this->load->view('template/header',$title);
        $this->load->view('viewReport', $data);
        $this->load->view('template/footer');
    }

    public function cetak(){
        ob_start();
        $data['slot_history'] = $this->ReportModel->tampil_data();
        $this->load->view('viewPrint', $data);
        $html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
        $pdf = new HTML2PDF('P','A4','en');
        $pdf->WriteHTML($html);
        $pdf->Output('Laporan Parkir.pdf', 'D');
    }
}