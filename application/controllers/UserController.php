<?php

/**
 * Created by PhpStorm.
 * User: agimaulana
 * Date: 19/05/17
 * Time: 3:58
 */
class UserController extends CI_Controller {


    public function updateToken(){
        $oldToken = $this->input->post("old_token");
        $newToken = $this->input->post("new_token");

        if(!isset($oldToken)){
            echo json_encode(array(
                "status" => false,
                "message" => "missing params"
            ));
            return;
        }

        $message = "";
        $data = array("fcm_token" => $newToken);
        if(!empty($oldToken)){
            $this->db->set($data)
                ->where('fcm_token', $oldToken)
                ->update('user');
            $message = "token updated";
        }else{
            $this->db->insert('user', $data);
            $message = "token inserted";
        }

        echo json_encode(array(
            "status" => true,
            "message" => $message
        ));
        return;
    }

    public function updateLocation(){
        $token = $this->input->post('fcm_token');
        $lat = $this->input->post('lat');
        $lng = $this->input->post('lng');
        $radius = $this->input->post('radius');

        if(!isset($token, $lat, $lng, $radius)){
            echo json_encode(array(
                "status" => true,
                "message" => "missing params"
            ));
            return;
        }

        $count = $this->db->from('user')
            ->where('fcm_token', $token)
            ->get()
            ->num_rows();
        if($count == 0){
            echo json_encode(array(
                "status" => false,
                "message" => "Token not found"
            ));
            return;
        }

        $data = array(
            'lat' => $lat,
            'lng' => $lng,
            'radius' => $radius
        );

        $this->db->set($data)
            ->where('fcm_token', $token)
            ->update('user');

        $userId = $this->db->select('id')
            ->from('user')
            ->where('fcm_token', $token)
            ->get()
            ->row()->id;
        $this->load->library('LatLng');
        $origin = new LatLng();
        $origin->setLatLng($lat, $lng);

        $listening = $this->db->select('id_location')
            ->from('user_listen')
            ->where('id_user', $userId)
            ->get()
            ->result();
        $listeningIds = array();
        foreach ($listening as $key => $value){
            $listeningIds[] = $value->id_location;
            $loc =  $this->db->select('latitude, longitude')
                ->from('lokasi')
                ->where('id_lokasi', $value->id_location)
                ->get()
                ->row();
            $destLatLng = new LatLng();
            $destLatLng->setLatLng($loc->latitude, $loc->longitude);
            $distance = $this->isInRadius($origin, $destLatLng);
            if($distance > $radius) {
                $this->db->where('id_location', $value->id_location)
                    ->delete('user_listen');
            }else{
                $this->db->set('distance', $distance)
                    ->where('id_location', $value->id_location)
                    ->update('user_listen');
            }
        }

        $addresses = $origin->getAddress();
        $locations = $this->db->select('*')
            ->from('lokasi')
            ->where('kota', $addresses['city']);
        if(sizeof($listeningIds) > 0)
            $locations = $locations->where_not_in('id_lokasi', $listeningIds);
        $locations = $locations->get()->result();
        foreach ($locations as $key => $value){
            /*$destLatLng = new LatLng();
            $destLatLng->setLatLng($value->latitude, $value->longitude);
            $distance = $destLatLng->distanceTo($latLng)['distance'];
            $distance = str_replace(",",".", $distance);
            if(strpos($distance, 'km')){
                $distance = $distance * 1000;
            }*/
            $destLatLng = new LatLng();
            $destLatLng->setLatLng($value->latitude, $value->longitude);
            $distance = $this->isInRadius($origin, $destLatLng);
            if($distance <= $radius) {
                $data = array(
                    'id_user' => $userId,
                    'id_location' => $value->id_lokasi,
                    'distance' => $distance
                );
                $this->db->insert('user_listen', $data);
            }
        }

        $this->load->model('ListenerBroadcaster');
        $broadcaster = new ListenerBroadcaster();
        $broadcaster->broadcastToUser($userId);

        echo json_encode(array(
            "status" => true,
            "message" => "location updated."
        ));
        return;
    }

    private function isInRadius(LatLng $origin, LatLng $destination){
        $distance = $origin->distanceTo($destination)['distance'];
        $distance = str_replace(",",".", $distance);
        if(strpos($distance, 'km')){
            $distance = $distance * 1000;
        }
        return $distance;
    }
}