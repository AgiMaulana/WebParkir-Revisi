<?php

/**
 * Created by PhpStorm.
 * User: agimaulana
 * Date: 17/03/17
 * Time: 2:28
 */
class LatLng {
    private $lat;
    private $lng;

    /**
     * LatLngDistance constructor.
     * @param $lat
     * @param $lng
     */
    /*public function __construct($lat, $lng)
    {
        $this->lat = $lat;
        $this->lng = $lng;
    }*/

    public function setLatLng($lat, $lng)
    {
        $this->lat = $lat;
        $this->lng = $lng;
    }

    /**
     * @return mixed
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @return mixed
     */
    public function getLng()
    {
        return $this->lng;
    }

    public function distanceTo(LatLng $latLng){
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$this->toString()
            ."&destinations=".$latLng->toString()."&mode=driving&language=id-ID";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
        $time = $response_a['rows'][0]['elements'][0]['duration']['text'];

        return array('distance' => $dist, 'time' => $time);
//        return $dist/1000;
    }

    public function getAddress(){
        // url encode the address
        $address = urlencode($this->toString());

        // google map geocode api url
        $url = "http://maps.google.com/maps/api/geocode/json?address={$address}";

        // get the json response
        $resp = $this->makerequest($url);

        // decode the json
//        $resp = json_decode($resp_json, true);
//
        // response status will be 'OK', if able to geocode given address
        if($resp['status']=='OK'){

            // get the important data
            $lati = $resp['results'][0]['geometry']['location']['lat'];
            $longi = $resp['results'][0]['geometry']['location']['lng'];
            $formatted_address = $resp['results'][0]['formatted_address'];
            $city = $resp['results'][sizeof($resp['results'])-3]['address_components'][0]['short_name'];
            $city = explode(" ", $city)[0];

            // verify if data is complete
            if($lati && $longi && $formatted_address){

                // put the data in the array
                return array(
                    "city" => $city,
                    "formatted" => $formatted_address
                );

            }else{
                return false;
            }

        }else{
            return false;
        }
    }

    public function toString(){
        return $this->lat .",". $this->lng;
    }

    private function makerequest($url) {
        $session = curl_init($url);
        curl_setopt($session, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($session);
        curl_close($session);
        return json_decode($response, true);
    }
}

/***
 *   How to use :
 *   $origin = new LatLng(<latitude>,<longitude>);
 *   $destination = new LatLng(<latitude>,<longitude>);
 *   $distance = $origin->distanceTo($destination);
 *   echo "Distance  : ". $distance['distance'];
 *   echo "\nTime    : ". $distance['time'];
 */