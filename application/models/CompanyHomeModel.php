<?php

/**
 * Created by PhpStorm.
 * User: agimaulana
 * Date: 18/06/17
 * Time: 12:41
 */
class CompanyHomeModel extends CI_Model {

    public function navBadge(){
        $token = $this->session->userdata('auth_token');
        $as = $this->session->userdata('as');

        $this->db->where("token", $token);
        if($this->db->count_all_results("company", false) == 0){
            redirect(base_url('logout'));
            return false;
        }
        $this->db->reset_query();

        $company = $this->db->select(['name','email','address'])
            ->from('company')
            ->where('token', $token)
            ->get()
            ->row();
        return $company;
    }

    public function locationsData(){
        $token = $this->session->userdata('auth_token');
        $as = $this->session->userdata('as');

        $this->db->where("token", $token);
        if($this->db->count_all_results("company", false) == 0){
            redirect(base_url('logout'));
            return false;
        }
        $this->db->reset_query();

        return $this->db->select('*')
            ->from('lokasi')
            ->join('company', 'company.company_id = lokasi.company_id')
            ->where('company.token', $token)
            ->get()
            ->result();
    }

    public function getCompanyId(){
        $token = $this->session->userdata('auth_token');
        $as = $this->session->userdata('as');

        $this->db->where("token", $token);
        if($this->db->count_all_results("company", false) == 0){
            redirect(base_url('logout'));
            return false;
        }
        $this->db->reset_query();

        return $this->db->select('company_id')
            ->from('company')
            ->where('token', $token)
            ->get()
            ->row()
            ->company_id;
    }

    public function deleteLocations($idCompany){
        $locations = $this->db->select('*')
            ->from('lokasi')
            ->where('company_id', $idCompany)
            ->get()
            ->result();
        foreach ($locations as $key => $value){
            $id_lokasi = $value->id_lokasi;
            $userIds = $this->db->select('user.id')
                ->from('user')
                ->join('user_listen', 'user_listen.id_user = user.id')
                ->join('slot', 'user_listen.id_location = slot.id_location')
                ->where('slot.id_location', $id_lokasi)
                ->group_by('user.id')
                ->get()
                ->result();

            $this->db->where('id_location', $id_lokasi)
                ->delete('slot');
            $where = array('id_lokasi' => $id_lokasi);
            $this->HomeModel->hapus_data($where,'lokasi');

            $this->load->model('ListenerBroadcaster');
            foreach ($userIds as $key => $value){
                $broadcaster = new ListenerBroadcaster();
                $broadcaster->broadcastToUser($value->id);
            }
        }
    }

    public function slots($locationId){
//        $hst->out = round(abs(strtotime($histories[$i+1]->datetime) - strtotime($hst->datetime)) / 60);
        $slots = $this->db->select('*')
            ->from('slot')
            ->where('id_location', $locationId)
            ->order_by('id_slot','ASC')
            ->get()
            ->result();

        $d = $this->input->get("date", true);
        if(!isset($d) || empty($d))
            $d = date("Y-m-d");
        foreach ($slots as $key => $value){
            $slot_histories = $this->db->select(['availablity','datetime as in'])
                ->from('slot_history')
                ->where('id_slot', $value->id_slot)
                ->like('datetime', $d)
                ->order_by('datetime', 'ASC')
                ->get()
                ->result();
            $days = array("Senin", "Selasa", "Rabu",
                "Kamis", "Jum'at", "Sabtu", "Minggu");

            $size = sizeof($slot_histories);
            for ($i = 0; $i < $size; $i++){
                $hst = $slot_histories[$i];
                if($i < $size-1 && $hst->availablity == 1){
                    $hstNext = $slot_histories[$i+1];
                    $hst->out = $hstNext->in;
                    $length = round(abs(strtotime($hstNext->in) - strtotime($hst->in)) / 60);
                    $hour = round($length/60);
                    $minute = $length - ($hour * 60);
                    if($minute < 0)
                        $hst->length = 0 . " jam ". $length . " menit" ;
                    else
                        $hst->length = $hour . " jam ". $minute . " menit" ;
                    $date = new DateTime($hst->in);
                    $hst->date = $days[$date->format("N")-1] . ", " . $date->format("d-m-Y");
                }

                if($hst->availablity == 0 || $i == $size-1)
                    unset($slot_histories[$i]);
            }

            $value->history = $slot_histories;
        }

        return $slots;
    }

}