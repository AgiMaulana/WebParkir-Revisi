<?php
class HomeModel extends CI_Model
{

	function tampil_data(){
	    $selection = array(
	        'company_id',
            'name',
            'email',
            'address',
            'active'
        );
        $companies = $this->db->select($selection)->get('company')->result();
        foreach ($companies as $key => $value){
            if($value->active == 0){
                $value->reset_link = base_url("admin/company/". $value->company_id ."/confirmation/send");
                $value->reset_label = 'Resend Confirmation';
            }else{
                $value->reset_link = base_url('/admin/company/'. $value->company_id .'/reset-password');
                $value->reset_label = 'Reset Password';
            }
        }

        return $companies;
    }

    function tampil_dt($nama_lokasi){
        return $this->db->get_where('lokasi', array('nama_lokasi'=>$nama_lokasi));
    }

    function edit_data($where,$table){
        return $this->db->get_where($table,$where);
    }

    function update_data($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }

    function hapus_data($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }

}
