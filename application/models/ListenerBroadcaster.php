<?php

/**
 * Created by PhpStorm.
 * User: agimaulana
 * Date: 09/06/17
 * Time: 8:54
 */
class ListenerBroadcaster extends CI_Model
{
    public function broadcastToUser($userId){
//        $userId = $this->input->get('user_id');
        $listens = $this->db->select('*')
            ->from('user_listen')
            ->join('lokasi', 'user_listen.id_location = lokasi.id_lokasi')
            ->where('user_listen.id_user', $userId)
            ->get()
            ->result();
        $response = array();
        foreach ($listens as $key => $value){
            $cameras = $this->db->select(['id_location', 'camera_ip','camera_name'])
                ->from('slot')
                ->where('id_location', $value->id_lokasi)
                ->group_by('camera_name')
                ->order_by('camera_name', 'ASC')
                ->get()
                ->result();
            $totalCount = 0;
            $availableCount = 0;
            foreach ($cameras as $camKey => $camValue){
                $slots = $this->db->select(['id_slot', 'availablity'])
                    ->from('slot')
                    ->where('id_location', $camValue->id_location)
                    ->where('camera_ip', $camValue->camera_ip)
                    ->order_by('id_slot', 'ASC')
                    ->get();
                $empty = $this->db->select(['id_slot', 'availablity'])
                    ->from('slot')
                    ->where('id_location', $camValue->id_location)
                    ->where('camera_ip', $camValue->camera_ip)
                    ->where('availablity', 0)
                    ->get()
                    ->num_rows();

                $availableCount += $empty;
                $camValue->total = $slots->num_rows();
                $totalCount += $camValue->total;
                $camValue->available = $empty;
                $camValue->slots = $slots->result();
            }
            $value->total = $totalCount;
            $value->available = $availableCount;
            $value->cameras = $cameras;
            $response[] = $value;
        }

        $token = $this->db->select('fcm_token')
            ->from('user')
            ->where('id', $userId)
            ->get()
            ->row()
            ->fcm_token;
        $this->load->model('PushModel');
        $push = new PushModel();
        $push->setLocations($response);
        $this->load->library('Firebase');
        $firebase = new Firebase();
        $firebase->send($token, $push);
//        echo json_encode($response);
    }
}