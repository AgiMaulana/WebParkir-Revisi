<?php
class LoginModel extends CI_Model
{

	function create($data){
		$this->db->insert("admin", $data);
	}

	function loginAsAdmin($user, $password){
        $where = array(
            'username' => $user,
            'password' => hash('sha1',$password),
        );
        $this->db->select('*')
            ->from('admin')
            ->where($where);
        $result = array();
        if($this->db->get()->num_rows() == 0){
            $result['status'] = false;
            $result['token'] = '';
        }else{
            $admin = $this->db->select('*')
                ->from('admin')
                ->where($where)
                ->get()->row();
            $token = $this->createAuthToken($admin->id, $user);
            $updateData = array('token' => $token);
            $this->db->reset_query();
            $this->db->where($where)->update('admin', $updateData);
            $result['status'] = true;
            $result['token'] = $token;
            $this->setSession($token, 'admin');
        }

        return $result;
    }

	function loginAsCompany($user, $password){
	    $where = array(
	        'email' => $user,
            'password' => hash('sha1',$password),
            'active' => 1
        );
        $this->db->select(array('company_id','email','password', 'token'));
        $this->db->where($where);
        $this->db->from('company');
        $result = array();
        if($this->db->get()->num_rows() == 0){
            $result['status'] = false;
            $result['token'] = '';
        }else{
            $this->db->select(array('company_id','email','password', 'token'));
            $this->db->where($where);
            $this->db->from('company');
            $company = $this->db->get()->row();
            $token = $this->createAuthToken($company->company_id, $user);
            $updateData = array('token' => $token);
            $this->db->reset_query();
            $this->db->where($where)->update('company', $updateData);
            $result['status'] = true;
            $result['token'] = $token;
            $this->setSession($token, 'company');
        }

        return $result;
    }

    private function setSession($token, $as){
        $newdata = array(
            'auth_token'  => $token,
            'as' => $as
        );

        $this->session->set_userdata($newdata);
    }

    private function createAuthToken($companyId, $email){
        $alpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $id = "";
        for ($i = 0; $i < 32; $i++){
            $rnd = rand(0, strlen($alpha)-1);
            $id.=$alpha[$rnd];
        }

        $key = hash('sha1', $companyId. $email);
        return hash('sha1', $key.$id);
    }
}
