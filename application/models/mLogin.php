<?php
class MLogin extends CI_Model
{

	function create($data){
		$this->db->insert("admin", $data);
	}
	
	function read($where="", $order=""){
		if(!empty($where)) $this->db->where($where);
		if(!empty($order)) $this->db->where($order);
		
		$query = $this->db->get("admin");
		
		if($query and $query->num_rows() != 0){
			return $query->result();
		}
		else{
			return array();
		}
	}
}
