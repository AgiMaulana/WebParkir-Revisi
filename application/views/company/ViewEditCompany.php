<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: agimaulana-->
<!-- * Date: 14/06/17-->
<!-- * Time: 23:29-->
<!-- */-->

<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-md-12">
                <!-- BASIC FORM ELELEMNTS -->
                <div class="content-panel">
                    <h4>Tambah Data Lokasi</h4>

                    <form class="form-horizontal style-form" method="post" action="<?=base_url()?>admin/company/edit/action" >
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Nama Perusahaan</label>
                            <div class="col-sm-10">
                                <input type="hidden" name="company_id" class="form-control" value="<?php echo $company_id ?>" required/>
                                <input type="text" name="company_name" class="form-control" placeholder="cth : PT Trusty Parking" value="<?php echo $name ?>" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">email</label>
                            <div class="col-sm-10">
                                <input type="email" name="email" id="email" class="form-control" value="<?php echo $email ?>" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Alamat</label>
                            <div class="col-sm-10">
                                <input type="text" name="address" class="form-control" value="<?php echo $address ?>" required/>
                            </div>
                        </div>

                        <input type="submit" id="submit" class="btn btn-primary" value="Simpan"/>

                    </form>

                </div>
            </div>
        </div><!--/row -->

    </section>
</section>