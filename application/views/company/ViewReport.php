<section id="main-content">
          <section class="wrapper">
             <div class="row">
				<div class="col-md-12">
						<div class="content-panel">
							<h3>Data Slot</h3>
							<h5><?= $location->nama_lokasi ?> (<?= $location->id_lokasi ?>)</h5>
							<h5><?= $location->alamat ?>, <?= $location->kota ?></h5>
						  <p></p>

							<div class="row slot-date">
								<form method="GET" action="<?php echo base_url("/company/location/". $location_id ."/report")?>">
									<div class="form-group">
										<label class="col-sm-1 col-sm-1 control-label">Tanggal</label>
										<div class="col-sm-6">
											<div class="input-group">
												<input class="form-control" id="date" name="date" placeholder="MM/DD/YYY" type="text" value="<?=$date?>" readonly/>
												<span class="input-group-btn">
												<input type="submit" class="btn btn-default" value="Lihat Slot"/>
										  	</span>
											</div><!-- /input-group -->
										</div>
									</div>
								</form>

							</div>

						  <p></p>
						  <div class="dataTable_wrapper">
							<table class="table table-striped " id="dataTables-example" style="color:#000; margin:15px; margin-left: 0px">
								<thead>
									<tr>	
										<th>No</th>
										<th>Hari/Tanggal</th>
                                        <th>ID Slot</th>
										<th>Waktu Masuk</th>
                                        <th>Waktu Keluar</th>
                                        <th>Lama Parkir</th>
									</tr>
								</thead>
								<tbody>
								<?php $i = 1; foreach ($slots as $key => $value){
										foreach ($value->history as $k => $v){?>

									<tr>
										<td style="text-align: center;"><?=$i?></td>
										<td><?=$v->date?></td>
										<td style="text-align: center;"><?=$value->id_slot?></td>
										<td><?=$v->in?></td>
										<td><?=$v->out?></td>
										<td><?=$v->length?></td>
									</tr>

								<?php }} ?>
								</tbody>
							</table>

							  <a href="/company/location/<?php echo $location->id_lokasi ?>/report/print?date=<?=$date?>" target="_blank"><div class="btn btn-info">Cetak</div></a>
						</div>	
                    </div>    
             </div>
          </section>
</section>

<script>
	$(document).ready(function(){
		var date_input=$('input[name="date"]'); //our date input has the name "date"
//		var container=$('.slot-date').length>0 ? $('.slot-date').parent() : "body";
		var options={
			format: 'yyyy-mm-dd',
			todayHighlight: true,
			autoclose: true
		};
		date_input.datepicker(options);
	})
</script>