<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	    <meta charset="utf-8">
        <link href="<?php echo base_url();?>assets/asset/css/table.css" rel="stylesheet">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Bootstrap Core CSS -->

<!--      <script type="text/javascript" src="--><?php //echo base_url().'assets/template/'; ?><!--bower_components/jquery/dist/jquery.min.js"></script>-->
<!--      <script type="text/javascript" src="--><?php //echo base_url().'assets/template/'; ?><!--bower_components/bootstrap/dist/js/bootstrap.min.js"></script>-->
<!--      <script type="text/javascript" src="--><?php //echo base_url().'assets/template/'; ?><!--bower_components/moment/min/moment.min.js"></script>-->
      <link rel="stylesheet" href="<?php echo base_url().'assets/template/'; ?>bower_components/bootstrap/dist/css/bootstrap.min.css"
      <link rel="stylesheet" href="<?php echo base_url().'assets/template/'; ?>bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css"/>

        <!-- Custom CSS -->
        <link href="<?php echo base_url();?>assets/asset/css/logo-nav.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
		
		    <!-- DataTables CSS -->
			<link href="<?php echo base_url().'assets/template/'; ?>bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

			<!-- DataTables Responsive CSS -->
			<link href="<?php echo base_url().'assets/template/'; ?>bower_components/datatables-responsive/css/responsive.dataTables.css" rel="stylesheet">
      <!-- js placed at the end of the document so the pages load faster -->
      <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
      <script class="include" type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
      <!-- <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js" type="text/javascript"></script> -->
      <script src="<?php echo base_url(); ?>assets/js/jquery.sparkline.js"></script>


      <!--common script for all pages-->
      <script src="<?php echo base_url(); ?>assets/js/common-scripts.js"></script>

      <!--script for this page-->
      <script src="<?php echo base_url(); ?>assets/js/sparkline-chart.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/zabuto_calendar.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'assets/template/'; ?>bower_components/moment/min/moment.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'assets/template/'; ?>bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

      <script type="application/javascript">
          $(document).ready(function () {
              $("#date-popover").popover({html: true, trigger: "manual"});
              $("#date-popover").hide();
              $("#date-popover").click(function (e) {
                  $(this).hide();
              });

              $("#my-calendar").zabuto_calendar({
                  action: function () {
                      return myDateFunction(this.id, false);
                  },
                  action_nav: function () {
                      return myNavFunction(this.id);
                  },
                  ajax: {
                      url: "show_data.php?action=1",
                      modal: true
                  },
                  legend: [
                      {type: "text", label: "Special event", badge: "00"},
                      {type: "block", label: "Regular event", }
                  ]
              });
          });


          function myNavFunction(id) {
              $("#date-popover").hide();
              var nav = $("#" + id).data("navigation");
              var to = $("#" + id).data("to");
              console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
          }

      </script>
      <!-- DataTables JavaScript -->
      <script src="<?php echo base_url().'assets/template/'; ?>bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
      <script src="<?php echo base_url().'assets/template/'; ?>bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
      <script>
          $(document).ready(function() {
              $('#dataTables-example').DataTable({
                  responsive: true
              });
          });
      </script>

      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a class="logo"><b>Monitoring Parkir</b></a>
            <!--logo end-->
			
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href= <?php echo base_url('logout'); ?> "LoginController/LOGOUT">Logout</a></li>
            	</ul>
            </div>
        </header>