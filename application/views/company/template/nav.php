      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><img src="<?php echo base_url(); ?>assets/img/ui-sam.jpg" class="img-circle" width="60"></p>
              	  <h5 class="centered"><?php echo $name ?></h5>
              	  <h5 class="centered"><?php echo $email ?></h5>
              	  <h5 class="centered"><?php echo $address ?></h5>

                  <li class="mt">
                      <a href="<?php echo base_url("company/dashboard"); ?>">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>
				  <li class="sub-menu">
                      <a href="<?php echo base_url("company/location/add"); ?>" >
                          <i class="fa fa-book"></i>
                          <span>Tambah Lokasi</span>
                      </a>
                  </li>
				  <li class="sub-menu">
                      <a href="<?php echo base_url("company/profile"); ?>" >
                          <i class="fa fa-book"></i>
                          <span>Profile</span>
                      </a>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->