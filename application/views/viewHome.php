<section id="main-content">
          <section class="wrapper">
             <div class="row">
				<div class="col-md-12">
						<div class="content-panel">
							<h4>Data Lokasi Parkir</h4>
						  <p></p>
						 
						  <p></p>
						  <div class="dataTable_wrapper">
							<table class="table table-striped " id="dataTables-example" style="color:#000; margin:15px; margin-left: 0px">
								<thead>
									<tr>	
										<th>No</th>
										<th>ID</th>
                                        <th>Nama Lokasi</th>
										<th>Kota</th>
                                        <th>Alamat</th>
                                        <th>Aksi</th>
									</tr>
								</thead>
								<tbody>
								<?php
									$no = 1;
									foreach ($company as $a) {
								?>
									<tr>
										<td style="text-align: center;"><?php echo $no++ ?></td>
										<td><?php echo $a->company_id ?></td>
										<td><?php echo $a->name ?></td>
                                        <td><?php echo $a->email ?></td>
										<td><?php echo $a->address ?></td>
										<td>
                                            <a href="<?php echo $a->reset_link ?>"><div type="button" class="btn btn-warning btn-xs"><?php echo $a->reset_label ?> <i class="glyphicon glyphicon-envelope"></i></div></a>
                                            <a href="company/<?php echo $a->company_id ?>/edit"><div type="button" class="btn btn-success btn-xs">Edit <i class="fa fa-pencil"></i></div></a>
                                            <a href="company/<?php echo $a->company_id ?>/delete"><div type="button" class="btn btn-danger btn-xs">Hapus <i class="glyphicon glyphicon-remove"></i></div></a>
                                        </td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
                    </div>
             </div><!--/row -->
             <!--Modal-->
             <div id="myModal" class="modal fade" role="dialog">
                 <div class="modal-dialog">
                     <div class="modal-content">
                         <div class="modal-header">
                             <button type="button" class="close" data-dismiss="modal">&times;
                             </button>
                             <h4 class="modal-title">Cetak Laporan</h4>
                         </div>
                         <div class="modal-body">
                             <p>Cetak Laporan pada Bulan :</p>
                             <form method="GET" action="<?php echo site_url('/status/jum_laporan');?>">
                             <select name="bulan" aria-controls="dataTables-example" class="form-control input-sm">
                                 <option value="">--Bulan--</option>
                                 <option value="01">Januari</option>
                                 <option value="02">Februari</option>
                                 <option value="03">Maret</option>
                                 <option value="04">April</option>
                                 <option value="05">Mei</option>
                                 <option value="06">Juni</option>
                                 <option value="07">Juli</option>
                                 <option value="08">Agustus</option>
                                 <option value="09">September</option>
                                 <option value="10">Oktober</option>
                                 <option value="11">November</option>
                                 <option value="12">Desember</option>
                             </select>
                                 <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal">Cetak Data</button>
                             </form>
                         </div>
                         <div class="modal-footer">
                             <button type="button" class="btn btn-warning" data-dismiss="modal">Tutup</button>
                         </div>
                     </div>
                 </div>
             </div>

             <script>
                 $('#myModal').modal('show');
             </script>
          </section>
</section>