<!DOCTYPE html>
<html lang="en">
<section id="main-content">
          <section class="wrapper">
             <div class="row">
				<div class="col-md-12">
					<style type="text/css">    
						  #map {
							margin: 10px;
							width: 600px;
							height: 300px;  
							padding: 10px;
							solid #DEEBF2;
						  }
						  
						  #map img { 
							  max-width: none;
							}

						  #map label { 
							  width: auto; display:inline; 
							}
						
					</style>
					<link href="<?php echo base_url();?>assets/css/center.css" rel="stylesheet">
					<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqgUKjGvp5Zs9UxPlbdIOFnGLiUi9lGRY&sensor=false" type="text/javascript"></script>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	       <div class="content-panel">
                  	  <h4>Tambah Data Lokasi</h4>				
					  
					  <form class="form-horizontal style-form" method="post" action="<?=base_url()?>index.php/InputController/aksi/<?=$aksi?>" >
						  <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Nama Lokasi</label>
                              <div class="col-sm-10">
                                  <input type="text" name="nama_lokasi" class="form-control" placeholder="cth : Fakultas Ilmu Terapan Tel-U" required/>
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Kota</label>
                              <div class="col-sm-10">
                                  <input type="text" name="kota" id="kota" class="form-control" placeholder="cth: Bandung" required/>
                              </div>
                          </div>

                          <div class="form-group">
                              <center><div id="map"></div></center>
                              <div align="center" class="tengah">
                                  <div class="col-sm-10">
                                      <div class="input-group">
                                          <input type="text" id="cari" class="form-control" placeholder="Search for..." >
                                          <span class="input-group-btn">
                                            <button class="btn btn-default" type="button" onclick="goToCity()">Go!</button>
                                        </span>
                                      </div><!-- /input-group -->
                                  </div>
                              </div>
                          </div>
						  
						  <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Latitude</label>
                              <div class="col-sm-10">
                                  <input type="text" name="latitude" id="latitude" class="form-control" placeholder="Latitude" readonly required/>
                              </div>
                          </div>

						  <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Longitude</label>
                              <div class="col-sm-10">
                                  <input type="text" name="longitude" id="longitude" class="form-control" placeholder="Longitude" readonly required/>
                              </div>
                          </div>

						  <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label" for="lokasi">Alamat</label>
                              <div class="col-sm-10">
                                  <input type="text" name="alamat" id="alamat" class="form-control" placeholder="cth : Jl. Telekomunikasi, No. 26, Bojongsoang" required/>
                              </div>
                          </div>
						  						  
						  <button type="submit" class="btn btn-warning" value="simpan" > simpan </button>
						  
                      </form>
					  
					  <script type="text/javascript">
                          var map;
                          var markerCount = 0;
                          function initMap() {
                              var myOptions = {
                                  zoom: 16,
                                  scaleControl: true,
                                  center: new google.maps.LatLng(0, 0),
                                  mapTypeId: google.maps.MapTypeId.ROADMAP
                              };
                              map = new google.maps.Map(document.getElementById("map"), myOptions);

                              if (navigator.geolocation) {
                                  navigator.geolocation.getCurrentPosition(function (position) {
                                      lat = position.coords.latitude;
                                      lng = position.coords.longitude;
                                      var pos = {
                                          lat: lat,
                                          lng: lng
                                      };
                                      printLog(pos);
                                      map.setCenter(pos);
                                  });
                              }

                              google.maps.event.addListener(map, 'mousemove', function(event) {
//                                  updateMarkerPosition(event.latLng);
                              });

                              google.maps.event.addListener(map, 'click', function(event) {
                                  updateMarkerPosition(event.latLng);
                                  if(markerCount == 0) {
                                      addMarker(event.latLng);
                                      markerCount = 1;
                                  }else{
                                      removeMarker();
                                      addMarker(event.latLng);

                                  }
                              });
                          }
                          google.maps.event.addDomListener(window, 'load', initMap);


                          //* Fungsi untuk mendapatkan nilai latitude longitude
						function updateMarkerPosition(pos) {
							document.getElementById('latitude').value = pos.lat();
							document.getElementById('longitude').value = pos.lng();
						}

						function printLog(pos){
                            console.log("Client lat : " +  pos.lat + ", lng : " +  pos.lng);
                        }

                        var marker;
                        function addMarker(pos) {
                            marker = new google.maps.Marker({
                                position: pos,
                                map: map
                            });
//                            getAddressByLatLng(pos.lat(), pos.lng());
                        }

                        function removeMarker() {
                            marker.setMap(null);
                        }
                        
                        function goToCity() {
                            var city = document.getElementById('cari').value;
                            console.log("Starting search LatLng of " + city );
                            var geocoder = new google.maps.Geocoder();
                            geocoder.geocode( { 'address': city}, function(results, status) {
                                if (status == 'OK') {
                                    var location = results[0].geometry.location;
                                    map.setCenter(location);
                                    map.resetZoom(17);
                                    console.log("Get LatLng of " + city + ", lat = " + location.lat() + ", lng = " + location.lng());
                                    /*var marker = new google.maps.Marker({
                                        map: map,
                                        position: results[0].geometry.location
                                    });*/
                                } else {
                                    console.log('Geocode was not successful for the following reason: ' + status);
                                }
                            });
                        }
                        
                        function getAddressByLatLng(lat, lng) {
                            var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
                            console.log("Starting search adress of " + lat + "," + lng );
                            var geocoder = new google.maps.Geocoder();
                            geocoder.geocode({'location': latlng}, function(results, status) {
                                if (status === 'OK') {
                                    if (results[1]) {
                                        document.getElementById('alamat').value = results[1].formatted_address;
                                        console.log("Get address of " + lat + "," + lng );
                                    } else {
                                        console.log('No results found');
                                    }
                                } else {
                                    console.log('Geocoder failed due to: ' + status);
                                }
                            });
                        }


						 
						/* buat marker yang bisa di drag lalu 
						  panggil fungsi updateMarkerPosition(latLng)
						 dan letakan posisi terakhir di id=latitude dan id=longitude
						 */
						 /*
						var marker = new google.maps.Marker({
							position : new google.maps.LatLng(-6.914744,107.609810),
							title : 'lokasi',
							map : map,
							draggable : true
						  });*/
						   
//						updateMarkerPosition(latLng);
						/*google.maps.event.addListener(marker, 'drag', function() {
						 // ketika marker di drag, otomatis nilai latitude dan longitude
						 //menyesuaikan dengan posisi marker
							updateMarkerPosition(marker.getPosition());
						  });*/


						</script>
					  					  
					</div>   
				</div> 
             </div><!--/row -->
			 
          </section>
</section>
</html>