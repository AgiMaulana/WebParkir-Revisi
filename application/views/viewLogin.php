<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

      <title>ParkingMonitoring::Login</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

      <style type="text/css">
          .form-login .form-login-heading {
              margin: 0;
              padding: 25px 20px;
              text-align: center;
              background: #424a5d;
              border-bottom: 5px solid #8ddb05;
              border-radius: 5px 5px 0 0;
              -webkit-border-radius: 5px 5px 0 0;
              color: #fff;
              font-size: 20px;
              text-transform: uppercase;
              font-weight: 300;
          }
      </style>
  </head>

  <body>

      <!-- ****************************************************************************************************************************************
      MAIN CONTENT
      ************************************************************************************************************************************ -->

	  <div id="login-page">
	  	<div class="container">
	  	
		      <form class="form-login" action="<?php echo base_url('login')?> " method="post">
                  <div class="form-login-heading">
                      <?php if($isFailed){ ?>
                        <h6>Username dan password tidak cocok.</h6>
                      <?php } ?>
		            <h2 >LOGIN</h2>
                  </div>
		        <div class="login-wrap">
		            <input name="user" type="text" class="form-control4" placeholder="Username" autofocus>
		            <br>
		            <input name="password" type="password" class="form-control4" placeholder="Password">
		            <br>
					<input type="submit" class="btn btn-theme btn-block" value="LOGIN"/>
		            	
		        </div>
		      </form>	  	
	  	
	  	</div>
	  </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("<?php echo base_url();?>assets/img/computer.jpg", {speed: 500});
    </script>


  </body>
</html>
