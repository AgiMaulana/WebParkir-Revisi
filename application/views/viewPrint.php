<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="content-panel">
                    <head>
                        <title>Cetak PDF</title>
                    </head>
                    <body>
                    <h1 style="text-align: center;">Data Siswa</h1>
                    <?php echo "Tanggal : ".date('d-m-Y'); ?>
                    <br>
                    <br>
                    <table border="1" width="100%">
                        <tr>
                            <th>No</th>
                            <th>Hari/Tanggal</th>
                            <th>Ketersediaan</th>
                        </tr>
                        <?php
                        $no = 1;
                        foreach ($slot_history as $a) {
                            ?>
                            <tr>
                                <td><?php echo $no++ ?></td>
                                <td><?php echo $a->time_millis ?></td>
                                <td><?php echo $a->availablity ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                    </body>
                </div>
            </div>
        </div>
    </section>
</section>