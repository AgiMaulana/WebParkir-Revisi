-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2017 at 03:05 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_parkir`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(10) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `id_lokasi` varchar(20) NOT NULL,
  `nama_lokasi` varchar(20) NOT NULL,
  `kota` varchar(10) NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `alamat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`id_lokasi`, `nama_lokasi`, `kota`, `latitude`, `longitude`, `alamat`) VALUES
('P04450', 'FRI', 'Bandung', '-6.975523252465732', '107.63119518756866', 'Jl. Telekomunikasi, Dayeuhkolot'),
('P08097', 'Fakultas Ilmu Terapa', 'Bandung', '-6.973004657262663', '107.63291046023369', 'Jl. Telekomunikasi, Dayeuhkolot'),
('P32096', 'FEB', 'Bandung', '-6.9715563257702025', '107.63200253248215', 'Jl. Telekomunikasi, Dayeuhkolot'),
('P81659', 'FIK', 'Bandung', '-6.972046203099609', '107.63120591640472', 'Jl. Telekomunikasi, Dayeuhkolot'),
('P84205', 'FTE', 'Bandung', '-6.9740003825663175', '107.63034224510193', 'Jl. Telekomunikasi, Dayeuhkolot');

-- --------------------------------------------------------

--
-- Table structure for table `slot`
--

CREATE TABLE `slot` (
  `id` int(11) NOT NULL,
  `id_location` varchar(20) NOT NULL,
  `camera` varchar(225) NOT NULL,
  `id_slot` varchar(10) NOT NULL,
  `time_millis` bigint(20) NOT NULL,
  `availablity` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `slot`
--
DELIMITER $$
CREATE TRIGGER `copy_slot_history_after_insert` AFTER INSERT ON `slot` FOR EACH ROW INSERT INTO slot_history (id_location, camera, id_slot, time_millis, availablity)
VALUES (NEW.id_location, NEW.camera, NEW.id_slot, NEW.time_millis, NEW.availablity)
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `copy_slot_history_after_update` AFTER UPDATE ON `slot` FOR EACH ROW INSERT INTO slot_history (id_location, camera, id_slot, time_millis, availablity)
VALUES (NEW.id_location, NEW.camera, NEW.id_slot, NEW.time_millis, NEW.availablity)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `slot_history`
--

CREATE TABLE `slot_history` (
  `id` int(11) NOT NULL,
  `id_location` int(11) NOT NULL,
  `camera` varchar(225) NOT NULL,
  `id_slot` int(11) NOT NULL,
  `time_millis` bigint(20) NOT NULL,
  `availablity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id_lokasi`);

--
-- Indexes for table `slot`
--
ALTER TABLE `slot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slot_history`
--
ALTER TABLE `slot_history`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `slot`
--
ALTER TABLE `slot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `slot_history`
--
ALTER TABLE `slot_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
